<link rel="stylesheet" href="style.css" >
<form class="railway" method="POST">
<div class="stripes-block">
<div class="line"></div>
</div>
<div class="form-group">
    <?php if (!empty($messages['OK']))print '<p>.'.$messages['OK'].'<p>'; ?>
    <label>
      Имя:
      <br>
      <input type="text" name="fio" <?php print 'value="'.$values['fio'].'"'?>>
        <?php
        if($errors['fio']) {print $messages['fio'];}
        ?>
    </label>
    <br>
    <label>
      E-mail:<br>
      <input type="email" name="email" <?php print 'value="'.$values['email'].'"'?>>
        <?php
        if($errors['email']) print $messages['email'];
        ?>
    </label>
    <br>
    <label>
      Дата рождения:<br>
      <input type="date" name="birthdate" <?php print 'value="'.$values['birthdate'].'"'?>>
        <?php
        if($errors['birthdate']) print $messages['birthdate'];
        ?>
    </label>
    <br>
    <label>
      Пол:<br>
    </label>
        <?php $floor = [
                1 => ['str' => 'М','attr' => ''],
                2 => ['str' => 'Ж','attr' => ''],
                3 => ['str' => 'предпочитаю не говорить','attr' => ''],
                4 => ['str' => 'не знаю','attr' => ''],
                5 => ['str' => 'лава','attr' => '']
        ];
        if(!empty($values['floor'])){
            $floor[$values['floor']]['attr'] = 'checked';
        }
        foreach($floor as $key => $val) {
            print '<label>
                    <input type="radio" name="floor" value="' . $key . '" ' . $val['attr'] . '>' . $val['str'] . '
                   </label>';
        }
        if($errors['floor']) print $messages['floor'];
        ?>
    <br>
<div class="limbs">
    <label>
      Количество конечностей:<br>
    </label>
      <?php $limbs = [
            '0' => '',
            '1' => '',
            '2' => '',
            '3' => '',
            '4' => '',
            'more4' => ''
        ];
        if (!empty($values['limbs'])){
            $limbs[$values['limbs']] = 'checked';
        }
        foreach($limbs as $key => $val){
            print '<label>
        <input type="radio" name="limbs" value="'.$key.'" '.$val.'>'.$key.'
      </label>';
        }
        if($errors['limbs']) print $messages['limbs'];
        ?>
</div>
    <br>
    <label>
      Сверхспособности:<br>
      <select name="superskills[]" multiple="multiple">
          <?php
          $user = 'u20946';
          $pass = '9860142';
          try {
              $db = new PDO('mysql:host=localhost;dbname=u20946', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
              $sql = "select * from superpower";
              $stmt = $db->prepare($sql);
              $stmt->execute();
              foreach($stmt as $row) {
                  if (in_array($row['sup_id'], $values['superskills'])) {
                      $mes = 'selected';
                  } else $mes = '';
                  print '<option value="' . $row['sup_id'] . '" ' . $mes . '>' . $row['s_rusdesc'] . '</option>';
              }
          }
          catch (PDOException $e) {
          print('Error : ' . $e->getMessage());
          exit();
          }?>
      </select>
        <?php
        if($errors['superskills']) print $messages['superskills'];
        ?>
    </label>
    <br>
    <label>
      Биография:<br>
      <textarea name="biography"><?php print $values['biography']?></textarea>
        <?php
        if($errors['biography']) print $messages['biography'];
        ?>
    </label>
    <br>
    <label class="checkbox" >
        с контрактом ознакомлен<input name="agree" type="checkbox" <?php if ($values['agree']=='on')print 'checked'?>>
    </label>
    <?php
    if($errors['agree']) print $messages['agree'];
    ?>
  </div>
  <div class="submit-block">
<div class="submit-button">
<i class="fa fa-train" aria-hidden="true"></i><br>
    <label>
      <input name="sub" type="submit" value="Отправить">
    </label>
  </div>
  </div>
  </form>